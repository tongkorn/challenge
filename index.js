const modal = document.querySelector('.modal');
const contactForm = document.querySelector('.contact__form')
const modalClose = document.querySelector('.modal__close-icon')
const doneButton = document.querySelector('.modal__button_type_close')
const contactButton = document.querySelector('.header__button')
const navContactButton = document.querySelector('.nav__contact-button')
const contactElement = document.querySelector('.contact')
const cardButtonNext = document.querySelector('.card__button-next')
const cardButtonPrev = document.querySelector('.card__button-prev')
const header = document.querySelector('.header')
const navMenu = document.querySelector('.nav__button')
const navBarTop = document.querySelector('.nav__bar-top')
const navBarMiddle = document.querySelector('.nav__bar-middle')
const navBarBottom = document.querySelector('.nav__bar-bottom')
const navMenuList = document.querySelector('.nav__menu-overlay')

let cardIndex = 1;

const nextCard = (n) => {
    showNextCard(cardIndex += n)
}

const prevCard = (n) => {
    showPrevCard(cardIndex += n)
}

const handleCardClick = (n) => {
    let cards = document.querySelectorAll('.card')
    if (n > cards.length) {
        cardIndex = 1;
    }
    if (n < 1) {
        cardIndex = cards.length
    }
    for (let i = 0; i < cards.length; i++) {
        cards[i].style.display = "none";
        cards[i].classList.remove('flip-top-bck');
        cards[i].classList.remove('flip-top-fwd');
    }
    return cards[cardIndex - 1]
}

const showPrevCard = (n) => {
    const card = handleCardClick(n)
    card.style.display = "block"
    card.classList.add('flip-top-fwd');

}
const showNextCard = (n) => {
    const card = handleCardClick(n)
    card.style.display = "block"
    card.classList.add('flip-top-bck');
}

const openModal = () => {
    modal.classList.add('modal_opened')
}

const closeModal = () => {
    modal.classList.remove('modal_opened')
}

const handleFormSubmit = (e) => {
    e.preventDefault();
    openModal()
}

const hideHeader = () => {
    if (document.body.scrollTop > 1500 || document.documentElement.scrollTop > 500) {
        header.style.opacity = 0
        header.style.zIndex = -1
    } else {
        header.style.opacity = 1
        header.style.zIndex = 2
    }
}

const toggleNav = () => {
    navBarTop.classList.toggle('change')
    navBarMiddle.classList.toggle('change')
    navBarBottom.classList.toggle('change')
    navMenuList.classList.toggle('nav__menu_opened')
}

const closeNavOnOverlay = (e) => {
    if (e.target.classList.contains('nav__menu_opened') || e.target.classList.contains('nav__link')) {
        toggleNav()
    }
}

window.addEventListener('scroll', hideHeader)
contactForm.addEventListener('submit', handleFormSubmit)
modalClose.addEventListener('click', closeModal)
doneButton.addEventListener('click', closeModal)
contactButton.addEventListener('click', () => contactElement.scrollIntoView({
    behavior: 'smooth'
}))
navContactButton.addEventListener('click', () => {
    contactElement.scrollIntoView({ behavior: 'smooth' })
    toggleNav()

})
cardButtonNext.addEventListener('click', () => nextCard(1))
cardButtonPrev.addEventListener('click', () => prevCard(-1))
navMenu.addEventListener('click', toggleNav)
navMenuList.addEventListener('click', (e) => closeNavOnOverlay(e))

showPrevCard(cardIndex);